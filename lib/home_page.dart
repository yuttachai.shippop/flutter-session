import 'package:flutter/material.dart';
import 'package:layout_1/home_four_page.dart';
import 'package:layout_1/home_three_page.dart';
import 'package:layout_1/home_two_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  double? _height;
  double? _width;
  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              color: Colors.green,
              height: 300,
              width: _width,
              child: Stack(
                children: [
                  Positioned(
                    top: 0,
                    child: Container(
                      color: Colors.blue,
                      height: 200,
                      width: _width,
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      color: Colors.red,
                      height: 150,
                      margin: EdgeInsets.all(10),
                      padding: EdgeInsets.all(10),
                      width: _width,
                      child: Column(
                        children: [
                          Container(
                            width: 100,
                            color: Colors.grey,
                            alignment: Alignment.center,
                            child: Text("Text1"),
                          ),
                          SizedBox(height: 5),
                          Container(
                            color: Colors.grey,
                            alignment: Alignment.center,
                            child: Text(
                              "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy",
                              textAlign: TextAlign.center,
                            ),
                          ),
                          SizedBox(height: 5),
                          OutlinedButton(
                            onPressed: () {
                              var route = MaterialPageRoute(
                                  builder: (context) => const HomeTwoPage());
                              Navigator.push(context, route);
                            },
                            child: Container(
                              width: 200,
                              child: Text(
                                "click",
                                style: TextStyle(color: Colors.white),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            style: ButtonStyle(
                              shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15),
                              )),
                              backgroundColor:
                                  MaterialStateProperty.all(Colors.blue),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              color: Colors.yellow.shade200,
              constraints: BoxConstraints(minHeight: 250),
              width: _width,
              child: Column(
                children: [
                  Container(
                    color: Colors.yellow,
                    width: double.infinity,
                    margin: EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy",
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            TextButton(
                              onPressed: () {
                                var route = MaterialPageRoute(
                                    builder: (context) =>
                                        const HomeThreePage());
                                Navigator.push(context, route);
                              },
                              child: Text("see all"),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  GridView.count(
                    primary: false,
                    shrinkWrap: true,
                    padding: EdgeInsets.all(10),
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    crossAxisCount: 2,
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.all(8),
                        color: Colors.teal[100],
                        child: const Text("He'd have you all unravel at the"),
                      ),
                      Container(
                        padding: const EdgeInsets.all(8),
                        color: Colors.teal[200],
                        child: const Text('Heed not the rabble'),
                      ),
                    ],
                  )
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              color: Colors.orange.shade200,
              constraints: BoxConstraints(minHeight: 250),
              width: _width,
              child: Column(
                children: [
                  Container(
                    color: Colors.orange,
                    width: double.infinity,
                    margin: EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy",
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            TextButton(
                              onPressed: () {
                                var route = MaterialPageRoute(
                                    builder: (context) => const HomeFourPage());
                                Navigator.push(context, route);
                              },
                              child: Text("see all"),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  GridView.count(
                    primary: false,
                    shrinkWrap: true,
                    padding: EdgeInsets.all(10),
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    crossAxisCount: 2,
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.all(8),
                        color: Colors.teal[100],
                        child: const Text("He'd have you all unravel at the"),
                      ),
                      Container(
                        padding: const EdgeInsets.all(8),
                        color: Colors.teal[200],
                        child: const Text('Heed not the rabble'),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
