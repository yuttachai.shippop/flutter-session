import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class HomeFourPage extends StatefulWidget {
  const HomeFourPage({Key? key}) : super(key: key);

  @override
  _HomeFourPageState createState() => _HomeFourPageState();
}

class _HomeFourPageState extends State<HomeFourPage> {
  double? _height;
  double? _width;
  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.blue,
        automaticallyImplyLeading: false,
        centerTitle: false,
        elevation: 0,
        title: Container(
          width: 100,
          alignment: Alignment.centerLeft,
          // color: Colors.red,
          child: Image.asset('assets/images/logo_1.png'),
        ),
      ),
      body: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Column(
          children: [
            Container(
              width: _width,
              height: 200,
              color: Colors.transparent,
              child: Stack(
                children: [
                  Container(
                    height: 160,
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(20),
                        bottomRight: Radius.circular(20),
                      ),
                    ),
                    child: Stack(
                      children: [
                        Align(
                          alignment: Alignment.topRight,
                          child: Image.asset(
                              "assets/images/defaultProfilePic.png"),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Column(
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  // Container(
                                  //   child: Text(
                                  //     "ยินดีต้อนรับสู่ SHIPPOP",
                                  //     style: TextStyle(
                                  //       fontSize: 20,
                                  //       color: Colors.white,
                                  //     ),
                                  //   ),
                                  // ),
                                  // SizedBox(height: 10),
                                  Container(
                                    // color: Colors.grey,
                                    child: Text(
                                      "ค้นหารายการพัสดุ",
                                      style: TextStyle(
                                        fontSize: 15,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  Column(
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                          color: const Color.fromARGB(
                                              101, 224, 224, 224),
                                          borderRadius:
                                              new BorderRadius.circular(15.0),
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              left: 15, right: 15),
                                          child: TextFormField(
                                            style: TextStyle(
                                              color: Colors.white,
                                            ),
                                            decoration: InputDecoration(
                                              border: InputBorder.none,
                                              hintText: 'กรอกรหัสติดตามพัสดุ',
                                              prefixIcon: Icon(
                                                Icons.search,
                                                color: Colors.white,
                                              ),
                                              labelStyle: TextStyle(
                                                color: Colors.white,
                                              ),
                                              hintStyle: TextStyle(
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      height: 80,
                      color: Colors.transparent,
                      alignment: Alignment.bottomCenter,
                      margin: EdgeInsets.fromLTRB(15, 15, 15, 10),
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(
                              height: 80,
                              margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 3,
                                    blurRadius: 5,
                                    offset: Offset(0, 3),
                                  ),
                                ],
                              ),
                              padding: EdgeInsets.all(10),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Text(
                                      "จุดส่งพัสดุ",
                                      style: TextStyle(
                                        fontSize: 20,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  Container(
                                    height: 80,
                                    alignment: Alignment.center,
                                    child: Image.asset(
                                        "assets/icons/location.png"),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Expanded(
                            child: Container(
                              height: 80,
                              margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 3,
                                    blurRadius: 5,
                                    offset: Offset(0, 3),
                                  ),
                                ],
                              ),
                              padding: EdgeInsets.all(10),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Text(
                                      "เช็คค่าส่ง",
                                      style: TextStyle(
                                        fontSize: 20,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  Container(
                                    height: 80,
                                    alignment: Alignment.center,
                                    child: Image.asset(
                                        "assets/icons/CheckPrice.png"),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: _height! * 0.25,
              width: _width,
              child: CarouselSlider(
                options: CarouselOptions(
                  autoPlayInterval: Duration(seconds: 5),
                  autoPlayAnimationDuration: Duration(seconds: 1),
                  autoPlay: true,
                  viewportFraction: 1.0,
                  aspectRatio: 2.0,
                  enlargeCenterPage: false,
                ),
                items: [
                  "assets/images/Banner01.png",
                  "assets/images/Banner02.png",
                  "assets/images/Banner03.png",
                ]
                    .map((item) => Container(
                          margin: EdgeInsets.all(10),
                          child: ClipRRect(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(0.0)),
                              child: Stack(
                                children: <Widget>[
                                  Image.asset(item,
                                      fit: BoxFit.cover, width: 1000.0),
                                ],
                              )),
                        ))
                    .toList(),
              ),
            ),
            Container(
              width: _width,
              color: Colors.transparent,
              padding: EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("ฟังชั่นใช้งาน"),
                  SizedBox(height: 10),
                  Container(
                    // color: Colors.red,
                    child: Row(
                      children: [
                        Expanded(
                          child: CustomCard(
                            text: "ปริ้นข้อความ",
                            imagePath: "assets/icons/printfreetext.png",
                          ),
                        ),
                        Expanded(
                          child: CustomCard(
                            text: "ปริ้นหน้าเว็บไซต์",
                            imagePath: "assets/icons/internet.png",
                          ),
                        ),
                        Expanded(
                          child: CustomCard(
                            text: "คำถามที่พบบ่อย",
                            imagePath: "assets/icons/faq.png",
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CustomCard extends StatelessWidget {
  CustomCard({
    Key? key,
    required this.imagePath,
    required this.text,
  }) : super(key: key);
  String text;
  String imagePath;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120,
      // color: Colors.green,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 80,
            alignment: Alignment.center,
            child: Image.asset(imagePath),
          ),
          Container(
            height: 40,
            child: Text(
              text,
              textAlign: TextAlign.center,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
    );
  }
}
