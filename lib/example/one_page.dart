import 'package:flutter/material.dart';

class OnePage extends StatefulWidget {
  OnePage(this.number, {Key? key}) : super(key: key);
  int number;
  @override
  _OnePageState createState() => _OnePageState(this.number);
}

class _OnePageState extends State<OnePage> {
  _OnePageState(this.numberInPage);
  int numberInPage;

  bool isLoading = true;
  callAPI() {
    numberInPage = 5;
    setState(() {
      isLoading = false;
    });
  }

  @override
  void initState() {
    callAPI();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("onepage"),
      ),
      body: isLoading
          ? Text("loading..")
          : Center(
              child: Text(
                widget.number.toString(),
              ),
            ),
    );
  }
}
