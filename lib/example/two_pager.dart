import 'package:flutter/material.dart';

class NewWidget extends StatelessWidget {
  NewWidget({
    Key? key,
    required double? width,
    this.color = Colors.grey,
    this.title = "",
  })  : _width = width,
        super(key: key);

  final double? _width;
  Color? color;
  String? title;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      width: _width,
      color: color,
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                child: Container(
                  color: Colors.green,
                  child: Text(
                    title!,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
              TextButton(
                  onPressed: () {
                    print("object2");
                  },
                  child: Text("See all"))
            ],
          ),
          Row(
            children: [
              Expanded(
                child: Container(
                  height: 200,
                  margin: EdgeInsets.fromLTRB(0, 10, 5, 10),
                  color: Colors.purple,
                ),
              ),
              Expanded(
                child: Container(
                  height: 200,
                  margin: EdgeInsets.fromLTRB(5, 10, 5, 10),
                  color: Colors.grey,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
